package sample.controllers;

import dataBase.DataBase;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.MapValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseController {

    @FXML
    private ComboBox<String> tableComboBox;
    @FXML
    private ComboBox<String> taskComboBox;
    @FXML
    private TableView<HashMap> tableView;
    @FXML
    private Button buttonAdd;

    private final DataBase dataBase = new DataBase();

    @FXML
    void initialize() throws SQLException {
        fillComboBox();
        fillTaskComboBox();
    }

    @FXML
    void fillTableView() throws SQLException {
        tableView.getColumns().clear();

        ArrayList<String> columnList = dataBase.getColumnList(tableComboBox.getValue());
        ArrayList<TableColumn> tableColumns = createColumns(columnList);

        setItemsInTableView(columnList, tableComboBox.getValue());
        setValuesColumns(tableColumns, columnList);
        addColumnsIntoTableView(tableColumns);
    }

    private void setItemsInTableView(ArrayList<String> columnNames, String tableName) throws SQLException {
        tableView.setItems(dataBase.getInfo(tableName, dataBase.getColumnCount(tableName), columnNames));
    }

    private void setValuesColumns(ArrayList<TableColumn> tableColumns, ArrayList<String> columnNames) {
        for (int i = 0; i < tableColumns.size(); i++)
            tableColumns.get(i).setCellValueFactory(new MapValueFactory<String>(columnNames.get(i)));
    }

    private void addColumnsIntoTableView(ArrayList<TableColumn> tableColumns) {
        for (TableColumn tableColumn : tableColumns) {
            tableView.getColumns().add(tableColumn);
        }
    }

    private ArrayList<TableColumn> createColumns(ArrayList<String> columnList) {
        ArrayList<TableColumn> tableColumns = new ArrayList<>();
        for (String s : columnList) {
            TableColumn tableColumn = new TableColumn<HashMap, Object>(s);
            tableColumn.setStyle("-fx-alignment: CENTER");
            tableColumns.add(tableColumn);
        }

        return tableColumns;
    }

    @FXML
    public void add() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/views/AddIntoDataBase.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("Добавление");
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(root));
        stage.show();
    }


    private void fillComboBox() throws SQLException {
        tableComboBox.setPromptText("Выбор таблицы...");
        ArrayList<String> tables = dataBase.getTableList();
        tableComboBox.setItems(FXCollections.observableArrayList(tables));
    }

    private void fillTaskComboBox() {
        taskComboBox.setPromptText("Задания...");
        taskComboBox.setItems(FXCollections.observableArrayList("Поданные заявления", "Абитуриенты получивших неудовлетворительно", "Абитуриенты не прошедшие по конкурсу"));
    }

    public void fillTaskTableView() {
        tableView.getColumns().clear();

        switch (taskComboBox.getValue()) {
            case "Поданные заявления":
                try {
                    displaySubmittedApplications();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "Абитуриенты получивших неудовлетворительно":
                tableView.getColumns().clear();
                ArrayList<String> columnsNames = new ArrayList<>();
                columnsNames.add("ФИО_абитуриента");
                columnsNames.add("год_рождения");
                columnsNames.add("код_группы");
                ArrayList<TableColumn> tableSpecialityColumnsArray = createColumns(columnsNames);
                try {
                    tableView.setItems(FXCollections.observableArrayList(dataBase.readData(3, columnsNames, "SELECT `fio`, `nomer lista`, `cod gruppi` FROM abiturienti WHERE `poluch ocenka`='2'")));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                setValuesColumns(tableSpecialityColumnsArray, columnsNames);
                addColumnsIntoTableView(tableSpecialityColumnsArray);
                break;
            case "Абитуриенты не прошедшие по конкурсу":
                try {
                    tableView.getColumns().clear();
                    columnsNames = new ArrayList<>();
                    columnsNames.add("ФИО_абитуриента");
                    tableSpecialityColumnsArray = createColumns(columnsNames);
                    tableView.setItems(FXCollections.observableArrayList(dataBase.readData(1, columnsNames, "SELECT fio FROM looser WHERE points>" + 11)));
                    setValuesColumns(tableSpecialityColumnsArray, columnsNames);
                    addColumnsIntoTableView(tableSpecialityColumnsArray);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    private void displaySubmittedApplications() throws SQLException {
        tableView.getColumns().clear();

        TableColumn tableColumn = new TableColumn<HashMap, Object>("поданные заявления");
        tableColumn.setStyle("-fx-alignment: CENTER;");
        tableView.setItems(FXCollections.observableArrayList(dataBase.readData(1, "поданные_заявления", "SELECT `name specialnosti` FROM `cpecialnost` ORDER BY `podannie zayavleniya` DESC")));
        tableColumn.setCellValueFactory(new MapValueFactory<String>("поданные_заявления"));
        tableView.getColumns().add(tableColumn);
    }
}
