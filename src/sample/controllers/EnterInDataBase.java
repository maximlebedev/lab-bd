package sample.controllers;

import dataBase.DataBase;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Main;

import java.io.IOException;
import java.sql.SQLException;

public class EnterInDataBase {

    @FXML
    private PasswordField password;
    @FXML
    private Text errorTextField;
    @FXML
    private javafx.scene.control.TextField login;

    private final DataBase dataBase = new DataBase();

    @FXML
    private void registration() {
        giveError("");
        if (login.getText().equals("") || password.getText().equals("")) {
            giveError("Поля ввода пустые");
        } else {
            try {
                if (dataBase.insertUser(login.getText(), password.getText())) {
                    informNewUserWindow();
                }
            } catch (SQLException throwable) {
                giveError("SQLException: не удалось добавить нового пользователя.");
            }
        }
    }

    @FXML
    private void enter() {

        giveError("");
        if (login.getText().equals("") || password.getText().equals("")) {
            giveError("Поля ввода пустые");
        } else {
            try {
                if (dataBase.findUser(login.getText(), password.getText())) {
                    Stage mainStage = Main.getStage();
                    mainStage.close();
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/views/DataBaseView.fxml"));
                    Parent root = fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setTitle("Редактор базы данных");
                    stage.setResizable(false);
                    stage.centerOnScreen();
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.setScene(new Scene(root));
                    stage.show();
                }
            } catch (SQLException | IOException throwable) {
                giveError("SQLException: не удалось найти пользователя.");
            }
        }
    }

    private void giveError(String message) {
        errorTextField.setText(message);
    }

    private void informNewUserWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("");
        alert.setHeaderText(null);
        alert.setContentText("Вы добавили пользователя в базу данных");
        alert.showAndWait();
    }
}
