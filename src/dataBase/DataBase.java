package dataBase;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBase implements MySQLDataBaseConfig {

    private static Connection connection;
    public static Statement statement;

    static {
        try {
            connection = getConnection();
            statement = getStatement();
        } catch (Exception ignored) {
        }
    }

    private static Connection getConnection() throws Exception {
        Connection connection;

        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        return connection;
    }

    private static Statement getStatement() throws SQLException {

        statement = connection.createStatement();

        return statement;
    }

    public boolean insertUser(String user, String password) throws SQLException {
        if (user.equals("") || password.equals("")) {
            String query = "INSERT INTO users (user, password) VALUES (" +
                    "\"" + user + "\", " +
                    "\"" + password + "\");";

            statement.executeUpdate(query);

            return true;
        } else {
            return false;
        }
    }

    public boolean findUser(String user, String password) throws SQLException {
        String query = "SELECT user, password FROM users WHERE user=\"" + user + "\"";
        ResultSet resultSet = statement.executeQuery(query);

        resultSet.next();
        return resultSet.getString("user").equals(user) && resultSet.getString("password").equals(password);
    }

    public ArrayList<String> getColumnList(String table) throws SQLException {
        ArrayList<String> tableField = new ArrayList<>();
        String query = "SHOW COLUMNS FROM `" + table + "`";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            tableField.add(resultSet.getString(1));
        }

        return tableField;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public ArrayList<String> getTableList() throws SQLException {
        ArrayList<String> tableList = new ArrayList<>();
        String query = "SELECT `TABLE_NAME` FROM `information_schema`.`TABLES` WHERE `TABLES`.`TABLE_SCHEMA` = 'admission-committee-of-the-educational-institution';";

        ResultSet resultSet = statement.executeQuery(query);
        int columns = resultSet.getMetaData().getColumnCount();

        while (resultSet.next()) {
            for (int i = 1; i <= columns; i++) {
                if (!resultSet.getString(i).equals("users")) {
                    tableList.add(resultSet.getString(i));
                }
            }
        }

        return tableList;
    }

    public ObservableList<HashMap> getInfo(String tableName, int columnCount, ArrayList<String> columnsNames) throws SQLException {
        return FXCollections.observableArrayList(readData(tableName, columnCount, columnsNames));
    }

    private ArrayList<HashMap> readData(String tableName, int columnCount, ArrayList<String> columnNames) throws SQLException {

        ArrayList<HashMap> hashMaps = new ArrayList<>();

        String query = "SELECT * FROM `" + tableName + "`";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            HashMap<String, String> dataBaseInfo = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                dataBaseInfo.put(columnNames.get(i - 1), resultSet.getString(i));
            }
            hashMaps.add(dataBaseInfo);
        }

        resultSet.close();

        return hashMaps;
    }

    public ArrayList<HashMap> readData(int columnCount, String columnName, String request) throws SQLException {
        ArrayList<HashMap> hashMaps = new ArrayList<>();
        ResultSet rs = statement.executeQuery(request);
        while (rs.next()) {
            HashMap<String, String> dataBaseInfo = new HashMap<>();
            dataBaseInfo.put(columnName, rs.getString(columnCount));
            hashMaps.add(dataBaseInfo);
        }
        rs.close();

        return hashMaps;
    }

    public ArrayList<HashMap> readData(int columnCount, ArrayList<String> columnNames, String request) throws SQLException {
        ArrayList<HashMap> hashMaps = new ArrayList<>();
        ResultSet rs = statement.executeQuery(request);
        while (rs.next()) {
            HashMap<String, String> dataBaseInfo = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                dataBaseInfo.put(columnNames.get(i - 1), rs.getString(i));
            }
            hashMaps.add(dataBaseInfo);
        }
        rs.close();

        return hashMaps;
    }

    public int getColumnCount(String table) throws SQLException {
        ArrayList<String> tableField = new ArrayList<>();

        String query = "SHOW COLUMNS FROM `" + table + "`";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            tableField.add(resultSet.getString(1));
        }

        return tableField.size();
    }
}
