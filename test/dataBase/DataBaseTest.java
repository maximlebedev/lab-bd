package dataBase;

import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class DataBaseTest {

    @Test
    public void correctLogin() {
        try {
            DataBase database = new DataBase();

            boolean result = database.findUser("root", "root");
            Assert.assertTrue(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void incorrectLogin() {
        try {
            DataBase database = new DataBase();

            boolean result = database.findUser("vasya", "qwerty");
            Assert.assertFalse(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void correctReg() {
        try {
            DataBase database = new DataBase();

            boolean result = database.findUser("vasya32", "432dsa");
            Assert.assertTrue(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void incorrectReg() {
        try {
            DataBase database = new DataBase();

            boolean result = database.findUser("", "");
            Assert.assertFalse(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}